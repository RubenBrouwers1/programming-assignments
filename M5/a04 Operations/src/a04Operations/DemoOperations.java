package a04Operations;

import java.util.Scanner;

public class DemoOperations {
    public static void main(String[] args) {
        Operations op = new Operations();
        Scanner sc = new Scanner(System.in);

        System.out.print("Please enter your first number: ");
        op.setNumberOne(sc.nextInt());
        System.out.print("Please enter your second number: ");
        op.setNumberTwo(sc.nextInt());

        System.out.println("The sum is " + op.sum());
        System.out.println("The difference is " + op.difference());
        System.out.println("The product is " + op.product());
        System.out.printf("The quotient is: %.2f", op.quotient());




    }
}
