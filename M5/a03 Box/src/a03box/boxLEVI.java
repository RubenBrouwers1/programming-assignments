/*
package a03box;

public class boxLEVI {
        private String type;
        private double length;
        private double width;
        private double height;

        public Box(String type, double length, double width, double height) {
            this.type = type;
            this.length = length;
            this.width = width;
            this.height = height;
        }

        public Box(String type, double length) {
            this.type = type;
            this.length = length;
        }

        public double surface() {
            if (type.compareTo("Cube") == 0)
                return 6 * Math.pow(length, 2);
            return  2 * (height * width + height * length + width * length);
        }

        public double volume() {
            if (type.compareTo("Cube") == 0)
                return Math.pow(length, 3);
            return length * width * height;
        }

        public double tapeLength() {
            //If I want to flex with the cool ternary operator:
            //return (isCube(b) ? 4 * length : length + height + width);
            if (type.compareTo("Cube") == 0)
                return  4 * length;
            return height + width * 2;
        }

        public String toString() {
            return String.format(
                    "Type: %s\n" +
                            "Length: %.1f cm\n" +
                            "Width: %.1f cm\n" +
                            "Height: %.1f cm\n" +
                            "Surface: %.1f cm²\n" +

        }
*/