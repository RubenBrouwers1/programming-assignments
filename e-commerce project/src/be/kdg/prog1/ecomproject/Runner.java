package be.kdg.prog1.ecomproject;

public class Runner {
    public static void main(String[] args) {
        IndustrialAndScienceProduct[] product_array = {
            new IndustrialAndScienceProduct("Multipurpose", true, true, 76.43F, "Dril & home tool kit"
                    , "The ultimate home tool kit, every man needs"),
            new IndustrialAndScienceProduct("IT", true, true, 23.00F, "Arduino uno",
                    "your entry to the unique Arduino experience"),
            new IndustrialAndScienceProduct(false, "Construction", true, false, 7.10F, "Drywall screws",
                    "Self Drilling Drywall Plastic anchor screws"),
            new FiltersAndFiltrationSystem( "Kitchen", false, false, 14.99F,
                    "Water replacement filters", "Brita Standard Replacement Filters for wtaer Dispensers", "Activated carbon",
                    false, true),
            new FiltersAndFiltrationSystem("Home", false, false, 113.21F, "Air Cleaner", "Provides whole home filtration for " +
                    "optimal home comfort", "Fiber", true, false),
            new FiltersAndFiltrationSystem("Kitchen", true, true, 175.00F, "Drinking Filtration System", "Under Sink 5-Stage Reverse Osmosis Drinking Filtration System", "Nickel", false, true, 10),
            new Laboratryproduct( true, "Laboratory", true,true,  39.99F,
                        "Jewelry cleaner", "Professional ultrasonic laboratory jewelry cleaner", true,
                        "clean jewelry"),
            new Laboratryproduct(false, "Multipurpose", true, true,  12.95F,
                        "Spray Bottle", "Refillable Empty Spray Bottle for Cleaning", false, "Cleaning"),
            new Laboratryproduct(false, "Multipurpose", true,false, 15.99F,
                        "Notebook", "A5 Wide Ruled Hardcover Writing Notebook", false,
                        "Notetaking"),
            new Laboratryproduct(false, "Storage", false, false, 7.68F, "Silica gel packets",
                        "Desiccant Pack Moisture Absorber Dehumidifier", false, "Shipping")};

        Costumer costumer = new Costumer(HumanInterface.Name());
        Sale purchases = HumanInterface.Input(costumer, product_array);
        System.out.println(purchases.toString());//Why is this needed?
        System.out.println(purchases.printInvoice());

        }
}
