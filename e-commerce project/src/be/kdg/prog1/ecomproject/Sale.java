package be.kdg.prog1.ecomproject;

import java.time.format.DateTimeFormatter;
import java.time.LocalDateTime;

public class Sale implements Taxable {

    private final static SalesLine[] SalesLines = new SalesLine[100];
    private final Costumer costumer;

    public Sale(Costumer costumer) {
        this.costumer = costumer;
    }

    public SalesLine SalesAlreadyInTable(int count) {

        if (count > SalesLines.length || count < 0 ) {
            return null;
        }
        return SalesLines[count];
    }

    public void PutSalesInTable(IndustrialAndScienceProduct prod, int quantity, int count) {

        if (count >= 0 && count < SalesLines.length && prod != null) {
            SalesLines[count] = new SalesLine(prod, quantity);
        }
    }

    public SalesLine[] getExistingSales() {
        return SalesLines;
    }

    public String toString() {
        System.out.printf("Costumer name: %s \n", costumer.getName());
        String End = "_______________________________________________________________________________";
        for (SalesLine SalesLines : SalesLines) {
            if (SalesLines != null) {
                System.out.print("Product: " + SalesLines.getProduct().getName() + ", Quantity: " + SalesLines.getChosenQuantity() + ", Price (ExVAT): " +
                        SalesLines.getPriceExcludingVat()+" \n");
            }
        }
        return End;
    }

    @Override
    public double getVatPercentage() {
        return IndustrialAndScienceProduct.VAT;
    }

    @Override
    public double getTax() {
        double finaltotVat = 0;
        for (SalesLine sale : SalesLines) {
            if (sale != null) {
                double subtotfintvar = (sale.getTax() * sale.getChosenQuantity());
                finaltotVat += subtotfintvar;
            }
        }
        double roundedfinaltotVat = Math.round(finaltotVat * 20F) / 20F;
        return roundedfinaltotVat;
        //I think this can be done a bit shorter, but idk how...
    }

    @Override
    public double getPriceIncludingVat() {
        return getPriceExcludingVat() + getTax();
    }

    @Override
    public double getPriceExcludingVat() {
        double finalPriceExV = 0;
        for (SalesLine sale : SalesLines) {
            if (sale != null) {
                double subtotfinpExV = (sale.getPriceExcludingVat() * sale.getChosenQuantity());
                finalPriceExV += subtotfinpExV;
            }
        }
        return finalPriceExV;
    }

    public String printInvoice() {
        String end = " ";
        final int whiteLines = 60;
        final int whiteLinesInvoide = 6;


        for (int i = 0; i < whiteLines; i++) {
            System.out.println();
        }
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm:ss");
        LocalDateTime now = LocalDateTime.now();

        System.out.print("------------------------------\n" +
                "(      o                     )\n" +
                "(       o                    )\n" +
                "(     ___                    )                   _____                _\n" +
                "(     | |                    )                  |_   _|              (_)\n" +
                "(     | |   GrandUstrial.com )                    | | _ ____   _____  _  ___ ___\n" +
                "(     |o|                    )                    | || '_ \\ \\ / / _ \\| |/ __/ _ \\\n" +
                "(    .' '.                   )                   _| || | | \\ V / (_) | | (_|  __/\n" +
                "(   /  o  \\                  )                   \\___/_| |_|\\_/ \\___/|_|\\___\\___|\n" +
                "(  :____o__:                 )                   ================================\n" +
                "(  '._____.'                 )\n" +
                "------------------------------\n\n");
        System.out.print("                                                                                                              " + dtf.format(now));

        System.out.printf("\n\n%129s\n", "GrandUstrial");
        System.out.printf("%129s\n", "BE 9725 188 486");
        System.out.printf("%129s\n", "Address: 483 Rue Bouhouille");
        System.out.printf("%129s\n", "Namur, 5030");

        System.out.printf("\nCostumer name: %s \n", costumer.getName());

        System.out.print("Address: 5 Nationalestraat\n         Antwerp, 2000\n");
        System.out.print("\n=================================================================================================================================");
        System.out.printf("\n%-27s%-63s%6s%10s  %-6s %-10s", "Name", "| Description", " | Quantity", "| € UNIT", "| VAT ", "| VATinc");
        System.out.print("\n=================================================================================================================================");
/*
//This still gives me the null pointer exception, I do not know why, but the other one works lol
        for (int i=0; i< sales.length; i++){
            if (sales != null) {
                System.out.printf("\n%-29s%-61s %7s     %8.2f %8.2f%8.2f", sales[i].getProduct().getName(), sales[i].getProduct().getDescription(),
                        sales[i].getChosenQuantity(), sales[i].getPriceExcludingVat(), sales[i].getTax(), sales[i].getPriceIncludingVat() * sales[i].getChosenQuantity());
            }
        }

 */
        for (SalesLine sale : SalesLines) {
            if (sale != null) {
                System.out.printf("\n%-29s%-61s %7s     %8.2f %8.2f%8.2f", sale.getProduct().getName(), sale.getProduct().getDescription(),
                        sale.getChosenQuantity(), sale.getPriceExcludingVat(), sale.getTax(), sale.getPriceIncludingVat() * sale.getChosenQuantity());
                //I was wondering about the decimal places, because right now, they round out to the closest number, is it oke this way, or does it have to be truncated?
                //I also was not sure about the 'VATinc' column. I suspected it had to be the price including the vat, multiplied by the chosen quantity
            }
        }
        for (int i = 0; i < whiteLinesInvoide; i++) {
            System.out.println();
        }

        System.out.print("\n=================================================================================================================================");
        System.out.printf("\n%-20s %107.2f", "Price excluding VAT", getPriceExcludingVat());
        System.out.printf("\n%-20s %107.2f", "VAT", getTax());
        System.out.printf("\n%-20s %107.2f", "Total", getPriceIncludingVat());
        System.out.print("\n=================================================================================================================================");
        System.out.printf("\n\n%79s", "Thank you for shopping here!");
        System.out.print("\n© Grandustrial.com");

        return end;
    }
}


