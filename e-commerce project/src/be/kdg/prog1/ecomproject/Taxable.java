package be.kdg.prog1.ecomproject;

public interface Taxable {
    double getVatPercentage();
    double getTax();
    double getPriceIncludingVat();
    double getPriceExcludingVat();
}
