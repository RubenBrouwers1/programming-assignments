package be.kdg.prog1.ecomproject;

public class SalesLine implements Taxable{
    private IndustrialAndScienceProduct product;
    private int chosenQuan;

    public SalesLine(IndustrialAndScienceProduct product, int chosenQuan) {
        this.product = product;
        this.chosenQuan = chosenQuan;
    }

    public IndustrialAndScienceProduct getProduct() {
        return product;
    }

    public int getChosenQuantity() {
        return chosenQuan;
    }

    public void setProduct(IndustrialAndScienceProduct product) {
        this.product = product;
    }

    public void setChosenQuan(int chosenQuan) {
        this.chosenQuan = chosenQuan;
    }

    public void incrementChosenQuantity(int chosenQuan){
        this.chosenQuan += chosenQuan;
    }


    public double getPriceExcludingVat() {
        return product.getPriceExcludingVat();
    }

    @Override
    public double getVatPercentage() {
        return IndustrialAndScienceProduct.VAT;
    }

    @Override
    public double getTax() {
        return getPriceExcludingVat() * (getVatPercentage()-1);
    }

    public double getPriceIncludingVat() {
        return product.getPriceExcludingVat() + getTax();
    }

    @Override
    public String toString() {
        String SalesLine = "%s has been sold %d times";
        return super.toString() + String.format(SalesLine, product.getName(), chosenQuan);
    }

    public void printInvoice() {
        System.out.printf("%s\n%d\n€%.2f\n€%.2f\n€.2f\n€.2\n", product.getName(), chosenQuan, product.getPriceIncludingVat(), getPriceExcludingVat(), getTax(), getPriceIncludingVat());
    }
}
