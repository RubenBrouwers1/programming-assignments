package be.kdg.prog1.m7.a03.products.clothes;

import be.kdg.prog1.m7.a03.products.Product;

public class Shirt extends Product {
    private String gender;
    private String size;

    public String getGender() {
        return gender;
    }

    public String getSize() {
        return size;
    }

    public Shirt(String gender, String size, String code, String description, float price) {
        super(code, description, price);
        this.gender = gender;
        this.size = size;
    }

    public double getVat(){
        return price*0.21;}

        @Override
        public String toString() {
            return String.format("Gender: %s, Size: %s, Price %.2f, Code: %s, Description: %s%n", getGender(), getSize(), getPrice(), getCode(), getDescription() );
    }
}