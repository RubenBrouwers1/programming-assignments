package vowels;

import java.util.Scanner;

public class Vowels {
    public static void main(String[] args) {
        Scanner sc =  new Scanner(System.in);

        int count = 0;
        String sentence;

        System.out.print("Please enter a sentence: ");
        sentence = sc.nextLine();

        for (int i = 0; i < sentence.length(); i++){
            Character chars = sentence.charAt(i);
            switch (chars){
                case 'a':
                case 'e':
                case 'i':
                case 'o':
                case 'u':
                case 'y':
                    count++;
                    break;
            }
        }

        System.out.printf("The string has %d vowels", count);
    }
}
