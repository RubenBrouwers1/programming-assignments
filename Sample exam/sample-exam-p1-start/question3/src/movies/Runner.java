package movies;

public class Runner {
	public static void main(String[] args) {
		Platform netFlix = new Platform();
		netFlix.addMovie(new Movie("Batman", 1989));
		netFlix.addMovie(new Movie("When Harry Met Sally", 1989));
		netFlix.addMovie(new Movie("Dances With Wolves", 1990));
		netFlix.addMovie(new Movie("Pretty Woman", 1990));
		netFlix.addMovie(new Movie("Total Recal", 1990));
		netFlix.addMovie(new Movie("Thelma & Louise", 1991));
		netFlix.addMovie(new Movie("The Silence Of The Lambs", 1991));
		netFlix.addMovie(new Movie("Reservoir Dogs", 1992));
		netFlix.addMovie(new Movie("Jurassic Park", 1993));
		netFlix.addMovie(new Movie("Schindler's List", 1993));

// TODO 3.1 Add the classes Movie and Platform as explained in README.md.
// TODO 3.2 uncomment the code above. If Movie and platform are correct you should not have errors in the above code
// TODO 3.1 Add code below to
//    Print every movie on the netFlix platform.
//    Print the number of movies that are older than 1990
//    Print if netFlix has capacity for additional movies

		for (int i = 0; i < netFlix.getNumberOfMovies(); i++) {
			if (netFlix.getMovie(i) != null) {
				System.out.println(netFlix.getMovie(i));
			}
		}

		System.out.println();
		int count = 0;

		for (int i = 0; i < netFlix.getNumberOfMovies(); i++) {
			if (netFlix.getMovie(i) != null) {
				if (netFlix.getMovie(i).getYear() < 1990) {
					count++;
				}
			}
		}
		System.out.printf("There are %d movies before the nineties\n", count);
		System.out.println(netFlix.isFull()? "There is more room" : "The array is full");
	}
}
