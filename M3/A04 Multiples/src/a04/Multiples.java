package a04;

import java.util.Scanner;

public class Multiples {
    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);

        int userInput;
        int maxNumber = 100;
        //int solution;

        System.out.print("Which number would you like to see the multiples of?: ");
        userInput = sc.nextInt();

        final int solidUserInput = userInput;

        while (userInput <= maxNumber) {
            System.out.println(userInput);
            userInput = userInput + solidUserInput;


        }
    }
}
