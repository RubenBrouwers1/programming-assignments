package a02;

import java.util.Scanner;

public class a02_Scrabble {
    public static void main(String[] args) {

        Scanner keyboard = new Scanner(System.in);

        System.out.print("Of what letter would you like to know the score of? (Only enter an Uppercase letter):");
        Scanner sc = new Scanner(System.in);
        char score = sc.next().charAt(0);
        System.out.println("You entered: " + score);

        switch((char) score) {
            case 'A':
                System.out.println("The scrabble value for that letter is 1");
                break;
            case 'E':
                System.out.println("The scrabble value for that letter is 1");
                break;
            case 'I':
                System.out.println("The scrabble value for that letter is 1");
                break;
            case 'O':
                System.out.println("The scrabble value for that letter is 1");
                break;
            case 'U':
                System.out.println("The scrabble value for that letter is 1");
                break;
            case 'L':
                System.out.println("The scrabble value for that letter is 1");
                break;
            case 'N':
                System.out.println("The scrabble value for that letter is 1");
                break;
            case 'S':
                System.out.println("The scrabble value for that letter is 1");
                break;
            case 'T':
                System.out.println("The scrabble value for that letter is 1");
                break;
            case 'R':
                System.out.println("The scrabble value for that letter is 1");
                break;
            case 'D':
                System.out.println("The scrabble value for that letter is 2");
                break;
            case 'G':
                System.out.println("The scrabble value for that letter is 2");
                break;
            case 'B':
                System.out.println("The scrabble value for that letter is 3");
                break;
            case 'C':
                System.out.println("The scrabble value for that letter is 3");
                break;
            case 'M':
                System.out.println("The scrabble value for that letter is 3");
                break;
            case 'P':
                System.out.println("The scrabble value for that letter is 3");
                break;
            case 'F':
                System.out.println("The scrabble value for that letter is 4");
                break;
            case 'H':
                System.out.println("The scrabble value for that letter is 4");
                break;
            case 'V':
                System.out.println("The scrabble value for that letter is 4");
                break;
            case 'W':
                System.out.println("The scrabble value for that letter is 4");
                break;
            case 'Y':
                System.out.println("The scrabble value for that letter is 4");
                break;
            case 'K':
                System.out.println("The scrabble value for that letter is 5");
                break;
            case 'J':
                System.out.println("The scrabble value for that letter is 8");
                break;
            case 'X':
                System.out.println("The scrabble value for that letter is 8");
                break;
            case 'Q':
                System.out.println("The scrabble value for that letter is 10");
                break;
            case 'Z':
                System.out.println("The scrabble value for that letter is 10");
                break;
            default:
                System.out.println("You made an invalid entry.");
        }

        //This is what I got when I didn't try the switch loop yet
        /*
        int a = 1, e = 1, i = 1, o = 1, u = 1, l = 1, n = 1, s = 1, t = 1, r = 1;
        int d = 2, g = 2;
        int b = 3, c = 3, m = 3, p = 3;
        int f = 4, h = 4, v = 4, w = 4, y = 4;
        int k = 5;
        int j = 8, x = 8;
        int q = 10, z = 10;
        */

        /*
        boolean point1 = false;
        boolean point2 = false;
        boolean point3 = false;
        boolean point4 = false;
        boolean point5 = false;
        boolean point8 = false;
        boolean point10 = false;
        */

        /*
        int userInput;
        int answer;

        System.out.print("Please enter a letter: ");
        userInput = keyboard.nextInt();
        */






    }
}
