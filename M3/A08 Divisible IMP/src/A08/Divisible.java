package A08;
import java.util.Scanner;

public class Divisible {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int MAX = 1000;

        boolean stop = false;

        do {
            System.out.print("The program stops with 0 \nPlease enter your first number: ");
            int num1 = scanner.nextInt();
            if (num1 > 0) {
                System.out.print("Enter your second number: ");
                int num2 = scanner.nextInt();
                if (num2 > 0) {
                    int count = 0;
                    for (int i = 1; i < MAX; i++) {
                        if (i % num1 == 0 && i % num2 == 0) {
                            System.out.print(i + " ");
                            if (++count % 5 == 0) {
                                System.out.println();
                                count = 0;
                            }
                        }
                    }
                    System.out.println();
                } else {
                    if (num2 < 0) {
                        System.out.print("Please enter a positive number!");
                    } else {
                        stop = true;
                    }
                }
            } else {
                if (num1 < 0) {
                    System.out.println("Please enter a positive number!");
                } else {
                    stop = true;
                }
            }
        } while (stop == false);

    }
}
