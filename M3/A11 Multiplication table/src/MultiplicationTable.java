import java.util.Scanner;

public class MultiplicationTable {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        int number;

        do {
            System.out.print("Enter a number between 1 and 30: ");
            number = sc.nextInt();
            if (number <=1 || number >=30){
                System.out.println("Invalid number");
            }
        }while(number <=1 || number >=30);

        for (int i=1; i<=number; i++){
            for(int j = 1; j<=5; j++){
                System.out.printf("|%3d", j*i);
            }
            System.out.println();
        }
    }
}
