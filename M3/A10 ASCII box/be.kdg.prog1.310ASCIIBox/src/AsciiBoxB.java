import java.util.Scanner;

public class AsciiBoxB {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        String character;
        int width;
        int height;

        System.out.println();
        System.out.println("-----------------------------------------------------------------------------------------");

        System.out.println("We'll draw an ASCII box using a character and dimensions of your choice.");

        System.out.print("Please enter a character: ");
        character = sc.nextLine();
        System.out.println();

        do {
            System.out.print("Enter the width (2..60): ");
            width = sc.nextInt();
            if (width > 60 || width < 2) {
                System.out.println("Invalid width");
            }
        } while (width > 60 || width < 2);
        System.out.println();
        System.out.printf("Your width is %d\n", width);
        System.out.println();

        do {
            System.out.print("Enter the height (2..60): ");
            height = sc.nextInt();
            if (height > 60 || height < 2) {
                System.out.println("Invalid height");
            }
        } while (height > 60 || height < 2);
        System.out.println();
        System.out.printf("Your height is %d\n", height);
        System.out.println();

        for (int j = 0; j <= height-1; ++j) {
            if (j == 0 || j == height-1) {
                for (int i = 1; i <= width; i++) {
                    System.out.print(character);
                }
                System.out.print("\n");
            }
            if(j != 0 && j != height-1) {
                int middle = width - 2;
                System.out.printf("%s", character);
                for (int k = 0; k < middle; k++) {
                    System.out.print(" ");
                }
                System.out.printf("%s\n", character);
            }
        }
    }
}
