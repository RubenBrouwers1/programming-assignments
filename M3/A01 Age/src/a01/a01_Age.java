package a01;

import java.util.Scanner;

public class a01_Age {
    public static void main (String args[]){
        //Your code comes here

        int userAge;
        Scanner keyboard = new Scanner(System.in);

        System.out.print("What is your age: ");
        userAge = keyboard.nextInt();

        if (userAge < 2) {
            System.out.println("You are a baby.");
        } else if (userAge > 2 && userAge < 12){
            System.out.println("You are a child.");
        } else if (userAge > 13 && userAge < 17){
            System.out.println("You are a teenager.");
        } else if (userAge > 18) {
            System.out.println("You are an adult.");
        } else {
            System.out.println("You have entered an invalid number.");
        }
    }
}
