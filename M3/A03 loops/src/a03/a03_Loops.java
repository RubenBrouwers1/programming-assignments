package a03;

public class a03_Loops {
    public static void main(String[] args) {

        //Task 1

        int i = 120;

        while ( i >= 100 ) {
            System.out.println(i);
            i = --i;
        }

        System.out.println("---------------------------------------------------------------------------------------- ");


        int number = 3;
        int count = 50;

        while (number <= count) {
            System.out.println(number);
            number = 3 + number;
        }
        System.out.println("---------------------------------------------------------------------------------------- ");

        int basenumber = 5;
        //long temp = 1;

        while (basenumber <= 10000) {
            System.out.println(basenumber);
            basenumber = basenumber * 5;
        }

        System.out.println("---------------------------------------------------------------------------------------- ");

        char c14 = 'a';

        while ( c14 <= 'z') {
            System.out.println(c14);
            c14 = ++c14;
        }

        System.out.println("---------------------------------------------------------------------------------------- ");

        //Task 2

        System.out.println("---------------------------------------------------------------------------------------- ");

        int x = 121;

        do {
            x = --x;
            System.out.println(x + " ");
            } while ( x >= 101);

        System.out.println("---------------------------------------------------------------------------------------- ");

        int number22 = 3;
        int count22 = 50;

        do {
            System.out.println(number22);
            number22 = 3 + number22;
        } while (number22 <= count22);

        System.out.println("---------------------------------------------------------------------------------------- ");

        int basenumber23 = 5;

        do {
           System.out.println(basenumber23);
           basenumber23 = basenumber23 * 5;
        } while (basenumber23 <=10000);

        System.out.println("---------------------------------------------------------------------------------------- ");

        char c24 = 'a';

        do {
            System.out.println(c24);
            c24 = ++c24;
        } while (c24 <= 'z');

        //Or in a shorter way
        /*
        char c24 = 'a';

        do {
            System.out.println(++c24);
        } while (c24 <= 'z');
         */

        System.out.println("---------------------------------------------------------------------------------------- ");

        //Task 3

        System.out.println("---------------------------------------------------------------------------------------- ");

        for (int y = 120; y >= 100; --y) {
            System.out.println(y + " ");
        }
        System.out.println("---------------------------------------------------------------------------------------- ");

        for (int i31 = 3; i31 < 50; i31++)
            if (i31 % 3 == 0)
                System.out.println(i31);

        System.out.println("---------------------------------------------------------------------------------------- ");


        for (int basenumber33 = 5; basenumber33 <= 10000; basenumber33 = basenumber33 * 5) {
            System.out.println(basenumber33);
        }


        System.out.println("---------------------------------------------------------------------------------------- ");

        char o;

        for (o = 'a'; o <= 'z'; ++o)
            System.out.println(o + " ");

        System.out.println("---------------------------------------------------------------------------------------- ");

    }
}
