package be.kdg.prog1.a15Garage;

public class Car {
    Garage garage;
    String brand;

    public Car(String brand) {
        this(null, brand);
    }

    public Car(Car car) {
        this(car.garage, car.brand);
    }

    public Car(Garage garage, String brand) {
        this.brand = brand;
        this.garage = garage;
    }

    public String getBrand() {
        return brand;
    }

    public Garage getGarage() {
        return garage;
    }

    public void setGarage(Garage garage) {
        this.garage = garage;
    }

    @Override
    public String toString() {
        return "Car: " + brand + " " + garage;
    }
}
