package Shape3D;

import java.lang.Math.*;

public class Cylinder extends Shape3D{

    protected double radius = 1.0;
    protected double length = 1.0;

    public Cylinder() {}

    public Cylinder(String colour, double radius, double length) {
        super(colour);
        this.radius = radius;
        this.length = length;
    }
    //check if this formula actually works
    public double surface() {
        return 2*Math.PI*radius*length+2*Math.PI*Math.pow(radius, 2);
    }

    public double volume() {
        return Math.PI * Math.pow(radius, 2) * length;
    }
}
