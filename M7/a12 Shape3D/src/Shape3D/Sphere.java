package Shape3D;

public class Sphere extends Shape3D{
    double radius = 1.0;

    public Sphere() {}

    public Sphere(String colour, double radius) {
        super(colour);
        this.radius = radius;
    }

    public Sphere(double radius) {
        this.radius = radius;
    }

    public double surface() {
        return 4 * Math.pow(radius, 2) * Math.PI;
    }

    public double volume() {
        return (4 / 3) * Math.PI* Math.pow(radius, 3);
    }

}
