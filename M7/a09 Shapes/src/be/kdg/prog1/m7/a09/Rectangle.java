package be.kdg.prog1.m7.a09;

public class Rectangle extends Shape{

    int height;
    int width;

    public Rectangle() {

    }

    public Rectangle(int x, int y) {
        super(x, y);
    }

    public Rectangle( int x, int y , int height, int width ) {
        super(x, y);
        this.height = height;
        this.width = width;
    }

    public double getArea() {
        return height*width;
    }

    public double getPerimeter() {
        return height*2+width*2;
    }
/*
    public String toString() {

        String name = getClass().toString();
        name = name.substring(name.lastIndexOf('.') + 1);

        return String.format("%s at (%d, %d) with perimeter %.2f and surface %.2f",
                name, x, y, getPerimeter(), getArea());
    }

 */

}
