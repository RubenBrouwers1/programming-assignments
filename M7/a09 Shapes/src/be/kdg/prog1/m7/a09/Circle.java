package be.kdg.prog1.m7.a09;

public class Circle extends Shape{
    String name = "Circle";
    protected int radius;

    public Circle(int radius) {
        super(0,0);
        this.radius = radius;
    }

    public Circle(int x, int y) {
        super(x, y);
    }

    public Circle(int x, int y, int radius) {
        super(x, y);
        this.radius = radius;
    }

    public double getArea() {
        return radius*radius*Math.PI;
    }

    public double getPerimeter() {
        return 2*radius*Math.PI;
    }

    /*
    @Override
    public String toString() {

        String name = getClass().toString();
        name = name.substring(name.lastIndexOf('.') + 1);

        return String.format("%s at (%d, %d) with perimeter %.2f and surface %.2f",
                name, x, y, getPerimeter(), getArea());

    }

     */
}
