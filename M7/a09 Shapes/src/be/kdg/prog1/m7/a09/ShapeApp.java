package be.kdg.prog1.m7.a09;

public class ShapeApp {
    public static void main(String[] args) {
        Circle c = new Circle(5);
        Rectangle r = new Rectangle(2,3, 4, 5);
        Square s = new Square(10);
        System.out.println(c.toString());
        System.out.println(r.toString());
        System.out.println(s.toString());
    }
}
