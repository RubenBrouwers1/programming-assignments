package be.kdg.prog1.m7.a09;

public class Square extends Rectangle {

    protected int size;

    public Square() {

    }

    public Square(int size) {
        super(0, 0);
        this.size = size;
    }

    public Square(int x, int y) {
        super(x, y);
    }

    public Square(int x, int y, int size) {
        super(x, y);
        this.size = size;
    }

    public void setSize(int size) {
        this.size = size;
    }

    @Override
    public double getArea() {
        return size*size;
    }

    @Override
    public double getPerimeter() {
        return size*4;
    }

    /*
    @Override
    public String toString() {

        String name = getClass().toString();
        name = name.substring(name.lastIndexOf('.') + 1);

        return String.format("%s at (%d, %d) with perimeter %.2f and surface %.2f",
                name, x, y, getPerimeter(), getArea());
    }

     */
}
