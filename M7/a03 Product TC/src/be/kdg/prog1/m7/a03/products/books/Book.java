package be.kdg.prog1.m7.a03.products.books;

import be.kdg.prog1.m7.a03.products.Product;

public class Book extends Product {
    private String title;
    private String author;

    public Book(String title, String author, String code, String description, float price) {
        super(code, description, price);
        this.title = title;
        this.author = author;
    }

    public String getTitle() {
        return title;
    }

    public String getAuthor() {
        return author;
    }

    public double getVat(){
        return price*0.06;
    }

    @Override
    public String toString() {
        return String.format("Title: %s, Author: %s, Price %.2f, Code: %s, Description: %s%n", getTitle(), getAuthor(), getPrice(), getCode(), getDescription() );
    }
}
