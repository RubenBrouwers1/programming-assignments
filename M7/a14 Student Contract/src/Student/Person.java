package Student;

public class Person {

    protected String name;
    String phone;

    public Person(String name, String email, String fixedNumber, String mobileNumber) {
        this.name = name;
        this.email = email;
        this.fixedNumber = fixedNumber;
        this.mobileNumber = mobileNumber;
    }

    @Override
    public String toString() {
        return String.format("Name: %s, ", name);
    }
}
