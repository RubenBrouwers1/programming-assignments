package be.kdg.prog1.a07Dog.animals;

public class Dog extends Animal {

    protected final String tagLine = "Like a dog in a bowling game";

    public Dog(String name, String breed, String colour, String chipNumber) {
        super(name, breed, colour, chipNumber);
    }

}
