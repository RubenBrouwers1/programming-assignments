package be.kdg.prog1.a07Dog.animals;

public class Animal {

    protected final String chipNumber;
    protected String name;
    protected String breed;
    protected String colour;

    public Animal(String name, String breed, String colour, String chipNumber) {
        this.name = name;
        this.breed = breed;
        this.colour = colour;
        this.chipNumber = chipNumber;
    }

    public String getChipNumber() {
        return chipNumber;
    }

    @Override
    public String toString() {
        return "Kind: " + getClass().getSimpleName() + ", Name: " + name;
    }
}
