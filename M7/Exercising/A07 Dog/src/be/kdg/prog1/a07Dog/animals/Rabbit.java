package be.kdg.prog1.a07Dog.animals;

public class Rabbit extends Animal {

    private boolean digs;
    private final String tagLine = "I'm an ice rabbit";

    public Rabbit(String name, String breed, String colour, String chipNumber, boolean digs) {
        super(name, breed, colour, chipNumber);
        this.digs = digs;
    }


}
