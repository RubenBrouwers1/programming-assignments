package Student;

public class Person {

    protected String name;
    String phone;

    public Person(String name, String phone) {
        this.name = name;
        this.phone = phone;
    }

    @Override
    public String toString() {
        return String.format("Name: %s, ", name);
    }
}
