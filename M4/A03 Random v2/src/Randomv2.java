import java.util.Random;

public class Randomv2 {
    public static void main(String[] args) {

        Random rd1 = new Random();

        for (int i = 1; i<=5; i++) {
            System.out.printf("%.2f ", rd1.nextDouble());
        }
        System.out.println();

        Random rd2 = new Random(42);
        Random rd3 = new Random(42);

        int max = 42;
        int min = 1;

        for (int i = 0; i < 10; i++) {
            System.out.printf("%d ", rd2.nextInt(42)+1);
            System.out.printf("%d ", rd3.nextInt(42)+1);
        }

    }
}
