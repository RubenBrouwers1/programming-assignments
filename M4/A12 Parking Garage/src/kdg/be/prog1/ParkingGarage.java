package kdg.be.prog1;

import java.util.Random;

public class ParkingGarage {
    public static void main(String[] args) {

        int[][][] parkingSpots = new int[2][2][15];

        Random rd = new Random();

        for (int i = 0; i < parkingSpots.length; i++) {
            for(int j = 0; j < parkingSpots[i].length; j++) {
                for (int k = 0; k < parkingSpots[i][j].length; k++){
                    parkingSpots[i][j][k]= rd.nextInt(10);
                }
            }
        }

        int spotsG1 = 0;
        int spotsG2 = 0;

        //Can you make these 2 loops into 1 loop?
        for(int j = 0; j < parkingSpots[0].length; j++) {
            for (int k = 0; k < parkingSpots[0][j].length; k++){
                spotsG1 = spotsG1 + parkingSpots[0][j][k];
            }
        }

        for(int j = 0; j < parkingSpots[1].length; j++) {
            for (int k = 0; k < parkingSpots[1][j].length; k++){
                spotsG2 = spotsG2 + parkingSpots[1][j][k];
            }
        }

/*
        for (int i = 0; i < parkingSpots.length; i++) {
            for(int j = 0; j < parkingSpots[i].length; j++) {
                for (int k = 0; k < parkingSpots[i][j].length; k++){
                    spotsG1 = spotsG1 + parkingSpots[0][j][k];
                    spotsG2 = spotsG2 + parkingSpots[1][j][k];
                }
            }
        }
*/

        System.out.println();
        //Maybe make the garage number automated?
        System.out.printf("Garage %d: used %d times\n", 1 , spotsG1);
        System.out.printf("Garage %d: used %d times", 2 , spotsG2);
        System.out.println();
        

        for (int i = 0; i < parkingSpots.length; i++) {
            System.out.println();
            System.out.printf("Parking garage %d\n", i+1);
            System.out.println("=================");
            for(int j = 0; j < parkingSpots[i].length; j++) {
                System.out.printf("Floor %d\n", j+1);
                System.out.println("-----------------");
                for (int k = 0; k < parkingSpots[i][j].length; k++){
                    System.out.printf("Spot %d = %d \n", k+1, parkingSpots[i][j][k]);
                }
                System.out.println();
            }
        }
    }
}
