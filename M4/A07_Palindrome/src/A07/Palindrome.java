package A07;
import java.util.Scanner;

public class Palindrome {
    public static void main(String[] args) {

    Scanner sc = new Scanner(System.in);

    String str;
    System.out.print("Please enter your word here: ");
    str = sc.nextLine();

        int startIndex = 0;
        int lastIndex = str.length() - 1;

        boolean result = true;

        while (true) {

            if (startIndex >= lastIndex) {
                break;
            }


            char first = str.charAt(startIndex);
            char last = str.charAt(lastIndex);

        /*if (first == ' ') {
            startIndex++;
            continue;
        }

        if (last == ' ') {
            lastIndex--;
            continue;
        }*/

            if (first != last) {
                result = false;
                break;
            }

            startIndex++;
            lastIndex--;

        }

        if (result) {
            System.out.println("Yes! It was");
        } else {
            System.out.println("Nope! it wasn't");
        }
    }
}
