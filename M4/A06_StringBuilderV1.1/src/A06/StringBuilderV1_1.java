package A06;
import java.util.Scanner;
import java.util.Random;


public class StringBuilderV1_1 {
    public static void main(String[] args) {
        /*
        Scanner sc = new Scanner(System.in);
        String name;
        int length;

        System.out.print("Please enter your first name and last name divided by a space: ");
        name = sc.nextLine();
        length = name.length();

        //Initials
        String NAME = name.toUpperCase();
        String[] split = NAME.split(" ");

        System.out.println("Your initials are: " + (split[0].charAt(0)) + split[1].charAt(0));

        //Reversed name
        String reverse = "";
        for (int i = length - 1; i >= 0; i--)
            reverse = reverse + name.charAt(i);

        System.out.println("Your name in reverse is: " + reverse);

        //Replace all 'e' by 'a', avoid "replace" and "replaceAll"

        

        //All characters in a random order
        Random rand = new Random();
        String nameCopy = name;
        String nameRandom = "";

        while (!nameCopy.isEmpty()) {
            //I do not really understand the following 2 lines
            int charPosition = rand.nextInt(length);
            char c = nameCopy.charAt(length);
            if (c != ' ') {
                nameRandom += c;
            }
            //What does this line do exactly?
            // !!   I GET AN ERROR    !!
            nameCopy = nameCopy.substring(0, charPosition) + nameCopy.substring(charPosition + 1);
            System.out.println("Your name with the letters in random order is " + nameRandom);

        }

         */
        System.out.print("Enter your first name and last name, separated by a space: ");
        Scanner scanner = new Scanner(System.in);
        String name = scanner.nextLine();
        StringBuilder builderOne = new StringBuilder();
        StringBuilder builderTwo = new StringBuilder(name);
        StringBuilder builderThree = new StringBuilder(name);
        StringBuilder builderFour = new StringBuilder();

        for (int i = 0; i < name.length(); i++) {
            if (i == 0 || name.charAt(i - 1) == ' ') {
                builderOne.append(name.charAt(i));
            }
        }

        builderTwo.reverse();

        while (builderThree.indexOf("e") != -1) {
            int positionOfE = builderThree.indexOf("e");
            builderThree.setCharAt(positionOfE, 'a');
        }

        StringBuilder sb = new StringBuilder(name);
        Random random = new Random();
        while (sb.length() > 0) {
            int index = random.nextInt(sb.length());
            builderFour.append(sb.charAt(index));
            //TODO: What does this do?
            sb.deleteCharAt(index);
        }

        System.out.println(builderOne);
        System.out.println(builderTwo);
        System.out.println(builderThree);
        System.out.println(builderFour);
    }
}
