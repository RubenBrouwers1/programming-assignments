import java.util.Scanner;

public class StringBuilderV3 {
    public static void main(String[] args) {
        final int AMOUNT = 5;
        Scanner scanner = new Scanner(System.in);
        StringBuilder builder = new StringBuilder();

        // Read AMOUNT words and append each word to the StringBuilder.
        // Use a for-loop and 'printf'.

        for (int i = 1; i <=  AMOUNT; i++) {
            System.out.printf("Enter word %d: ", i);
            builder.append(scanner.nextLine() + " ");
        }

        System.out.println();

        // Print the content of 'builder'.

        System.out.println("Content of builder: " + builder);

        // Create a copy of the StringBuilder object and name it 'copy'. Make sure it contains
        // the same content as the original StringBuilder.
        // Print the content of 'copy'.

        StringBuilder copy = builder;
        System.out.println("Content of copy: " + copy);

        System.out.println();


        // Now check if 'builder' has the same content as 'copy'. Try '==' as well as the 'equals' method.
        // Note: unlike 'String', 'StringBuilder' doesn't actually have an implementation of the 'equals'
        // method. Yet, we can still call the 'equals' method on objects of type StringBuilder.
        // This will be explained later on! (You might want to take a look at the 'Object' class.)

        boolean isEqual1 = builder == copy;
        boolean isEqual2 = builder.toString().equals(copy.toString());

        System.out.printf("Comparison with == results in: %b\n", isEqual1);
        System.out.printf("Comparison with .equals results in: %b\n", isEqual2);

        System.out.println();

        // Convert builder to upper case without using the String class and without creating
        // a new StringBuilder.
        // Hint: use an ASCII table.

        for (int i = 0; i < builder.length(); i++) {
            if (builder.charAt(i) != ' '){
                char c = builder.charAt(i);
                c -= 32;
                builder.setCharAt(i, c);
            }
        }

        System.out.println(builder);

    }
}
