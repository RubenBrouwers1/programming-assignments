package a01Book;

public class Book {
    String Author;
    String Title;
    int Pages;
    boolean OnLoan;

    public Book(String author, String title, int pages, boolean onloan) {
        this.Author = author;
        this.Title = title;
        this.Pages = pages;
        this.OnLoan = onloan;
    }

    public Book() {
        Author = "Unknown";
        Title = "Unknown";
        Pages = 0;
    }

    public String getAuthor() {
        return Author;
    }

    public void setAuthor(String author) {
        Author = author;
    }

    public String getTitle() {
        return Title;
    }

    public void setTitle(String title) {
        Title = title;
    }

    public int getPages() {
        return Pages;
    }

    public void setPages(int pages) {
        Pages = pages;
    }

    public boolean isOnLoan() {
        return OnLoan;
    }

    public void setOnLoan(boolean onLoan) {
        OnLoan = onLoan;
    }

    public String toString() {
        return ""
    }
}

