package A02;
import java.util.Random;

public class RandomV1 {
    public static void main(String[] args) {
        Random rand = new Random();

        for (int i = 0; i < 6; i++) {
            int numb = rand.nextInt(6) + 1;
            System.out.print(numb + "\t");
        }

        System.out.println("");

        for (int i = 0; i < 6; i++) {
            boolean bool = rand.nextBoolean();
            System.out.print(bool + "\t");
        }

        System.out.println("");

        for (int i = 0; i < 3; i++) {
            double doub = rand.nextDouble();
            System.out.print(doub + "\t");
        }
        //Here I started with the preparation for the additionals, but I did not finish them.
/*
        System.out.println(" \n");

        System.out.println("Here begins the second part.");

 */
    }
}



