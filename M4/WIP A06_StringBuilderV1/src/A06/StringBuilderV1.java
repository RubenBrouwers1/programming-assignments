package A06;
import java.util.Scanner;
import java.util.Random;

public class StringBuilderV1 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        String name;

        System.out.print("Please enter your first name and your last name divided by a space: ");
        name = sc.nextLine();

        String NAME = name.toUpperCase();
        String[] split = NAME.split(" ");
        System.out.println("Your initials are: " + (split[0].charAt(0)) + split[1].charAt(0));

        String reverse = "";
        for(int i = name.length() - 1; i >= 0; i--)
        {
            reverse += name.charAt(i);
        }
        System.out.println("Your name backwards is: " + reverse);

        //Replace each 'e' with an 'a', do not use 'replace'

        StringBuffer result = new StringBuffer();
        int n = name.length();
        Random rand = new Random();
        while ( n > 1 )
        {
            int randomPoint = rand.nextInt( n );
            char randomChar = name.charAt( randomPoint );
            result.setCharAt( n-1, randomChar );
            n--;
        }




    }
}
