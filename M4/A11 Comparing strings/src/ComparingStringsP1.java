public class ComparingStringsP1 {
        public static void main(String[] args) {
            String literal = "Harry";
            String newReference = "Harry";
            String newObject = new String("Harry");

            // Check if literal and newReference are the same.
            // Compare them using ==.

            System.out.println(literal == newReference);

            // Check if literal and newObject are the same.
            // Compare them using ==.

            System.out.println(literal == newObject);

            // Check if literal and newReference are the same.
            // Compare them using the 'equals' method of String.

            boolean comp1 = literal.equals(newReference);
            System.out.println(comp1);

            // Check if literal and newObject are the same.
            // Compare them using the 'equals' method of String.

            boolean comp2 = literal.equals(newObject);
            System.out.println(comp2);

        }
    }
