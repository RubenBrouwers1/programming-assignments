import java.util.Scanner;

public class ComparingStringsP2 {
    public static void main(String[] args) {
        String stringOne;
        String stringTwo;

        // Read both strings using a Scanner.

        Scanner sc = new Scanner(System.in);

        System.out.print("First string: ");
        stringOne = sc.nextLine();

        System.out.print("Second string: ");
        stringTwo = sc.nextLine();

        // Trim both strings.

        stringOne = stringOne.trim();
        stringTwo = stringTwo.trim();

        // Compare them and print them in alphabetical order.

        int compare = stringOne.compareTo(stringTwo);

        if (compare < 0 ) {
            System.out.printf("%s %s", stringOne, stringTwo);
        } else if (compare > 0) {
            System.out.printf("%s %s", stringTwo, stringOne);
        } else {
            System.out.printf("%s is the same as %s", stringOne, stringTwo);
        }
    }
}
