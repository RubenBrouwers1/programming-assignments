import java.util.Locale;

public class ComparingStringsP3 {
    public static void main(String[] args) {
        String stringOne = "java";
        String stringTwo = "Java";
        String stringThree = "JAVA";

        boolean firstComp = false, thirdComp = false, secondComp= false;

        // Complete this program

        if (stringOne.compareToIgnoreCase(stringTwo) == 0) {
            firstComp = true;
        }

        if (stringOne.compareToIgnoreCase(stringThree) == 0) {
            secondComp = true;
        }

        if (stringTwo.compareToIgnoreCase(stringThree) == 0) {
            thirdComp = true;
        }

        System.out.printf("%s and %s are %s\n", stringOne, stringTwo, (firstComp ? "equal" : "not equal"));
        System.out.printf("%s and %s are %s\n", stringOne, stringThree, secondComp ? "equal" : "not equal");
        System.out.printf("%s and %s are %s\n", stringTwo, stringThree, thirdComp ? "equal" : "not equal");
    }
}
