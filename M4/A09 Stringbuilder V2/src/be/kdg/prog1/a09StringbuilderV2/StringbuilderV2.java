package be.kdg.prog1.a09StringbuilderV2;

import java.util.InputMismatchException;
import java.util.Scanner;

public class StringbuilderV2 {
    public static void main(String[] args) {
        StringBuilder sb = new StringBuilder();
        Scanner sc = new Scanner(System.in);

        final int max = 20;
        int number = 0;
        boolean correctNumb = false;

        System.out.print("Enter the highest number (1..20): ");

        do {
            try {
                 number = sc.nextInt();
                    if (number > max || number < 1) {
                        System.out.print("Your number is out of range, please try again: ");
                        correctNumb = false;
                    } else {
                        correctNumb = true;
                    }
                ;
            }catch (InputMismatchException e) {
                System.out.print("You did not enter a number, please try again: ");
                sc.next();
            }
        } while (!correctNumb);

        for (int i = 1; i <= number; i++){
            sb.append(i + " ");
        }

        System.out.println(sb);

        for (int i = 0; i < sb.length(); i++){
            if (sb.charAt(i) == ' '){
                sb.deleteCharAt(i);
            }
        }

        System.out.println(sb);
    }
}
