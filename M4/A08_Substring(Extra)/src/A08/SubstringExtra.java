package A08;

import java.util.Scanner;

public class SubstringExtra {
    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);

        int count = 0;
        String counter;
        String sentence;

        System.out.print("Please enter your sentence: ");
        sentence = sc.nextLine();
        System.out.println("Please enter what occurrence you would like to count: ");
        counter = sc.nextLine();

        StringBuilder sb = new StringBuilder();
        sb.append(sentence);
        String str = sb.toString();
        String[] p = str.split(" ");

        for (var item : p) {
            if (item == counter) {
                count++;
            }
        }

        System.out.printf("There are %d occurrences of \"%s\" in the sentence \"%s\"", count, counter, sentence);
    }
}
