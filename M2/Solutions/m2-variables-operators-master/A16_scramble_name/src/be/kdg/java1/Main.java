package be.kdg.java1;

import java.util.Random;
import java.util.Scanner;

public class Main {
    /*
    Enter your name: Lars Willemsens
    Hi Lars Willemsens, your scrambled name is snraeissWLmell.
    */
    /*
    Enter your name: Jan de Rijke
    Hi Jan de Rijke, your scrambled name is iRanjJedek.
    */
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        Random random = new Random();
        System.out.print("Enter your name: ");
        String name = scanner.nextLine();
        String nameCopy = name;
        String scrambledName = "";
        while (!nameCopy.isEmpty()) {
            int charPosition = random.nextInt(nameCopy.length());
            char c = nameCopy.charAt(charPosition);
            if (c != ' ') {
                scrambledName += c;
            }
            nameCopy = nameCopy.substring(0, charPosition) + nameCopy.substring(charPosition + 1);
        }
        System.out.printf("Hi %s, your scrambled name is %s.", name, scrambledName);
    }
}
