package be.kdg.java1;

public class Main {
    public static void main(String[] args) {
        for (char c = 32; c < 128; c++) {
            System.out.printf("    %c (%3d)", c, (int) c);

            if (c % 6 == 1) {
                System.out.println();
            }
        }
    }
}
