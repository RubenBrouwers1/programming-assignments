package ASCII;

public class ASCII {
    public static void main(String[] args) {

        for (int asciiNum = 32, counter = 1; asciiNum <= 255; asciiNum++, counter++) {

            System.out.print((char)asciiNum + " " + (asciiNum <= 100 ? " ( " : " (") + asciiNum + ")\t");

            if (counter % 6 == 0) {
                System.out.println();
            }

        }
    }
}
