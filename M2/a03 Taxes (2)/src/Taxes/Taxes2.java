package Taxes;

import java.util.Scanner;

public class Taxes2 {
    public static void main(String[] args) {

        Scanner keyboard = new Scanner(System.in);

        double vat;
        double amount;
        double vatTwo;
        double amountTwo;
        double incVat;
        double exVat;
        boolean included = false;

        while (included == false) {
            System.out.print("What is the amount you want to add VAT to?: ");
            amount = keyboard.nextDouble();
            System.out.print("What is the vat percentage? (Without %)?: ");
            vat = keyboard.nextDouble();
            exVat = amount * ((vat / 100) + 1);

            System.out.print("Your amount with VAT is " + exVat);
            System.out.println(" ");

            System.out.print("Would you calculate an amount where the VAT is included? (yes or no): ");
            String UserChoice = keyboard.next();
            if (UserChoice.equals("yes")) {
                included = true;
            } if (UserChoice.equals("no")) {
                System.out.println("the program will be terminated.");
                System.exit(0);
            }
        }
        while (included = true) {
            System.out.print("What is the amount you want to add VAT to?: ");
            amountTwo = keyboard.nextDouble();
            System.out.print("What is the vat percentage? (Without %)?: ");
            vatTwo = keyboard.nextDouble();
            incVat = amountTwo - (amountTwo * (vatTwo / 100));
            System.out.println("The amount without the VAT equals to " + incVat);
        return;
        }
    }
}