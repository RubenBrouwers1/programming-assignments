package Variables;

public class Variables {
    public static void main(String[] args) {

        boolean firstVar = true;
        char secondVar = 65;
        byte thirdVar = 50;
        short fourthVar = 456;
        int fifthvVar = -5;
        long sixtVar = -6;
        float seventVar = 56;
        double lastVar = 45.6 ;

        System.out.println("Print firstVar: " + firstVar);
        System.out.println("Print secondVar: " + secondVar);
        System.out.println("Print thirdVar: " + thirdVar);
        System.out.println("Print fourthVar: " + fourthVar);
        System.out.println("Print fifthvVar: " + fifthvVar);
        System.out.println("Print sixtVar: " + sixtVar);
        System.out.println("Print seventVar: " + seventVar);
        System.out.println("Print lastVar: " + lastVar);

        return;

    }
}
