package NumbersV1;

import java.sql.SQLOutput;

public class NumbersV1 {
    public static void main(String[] args) {

        //This is the first part
        double firstNumber = 2000000000;
        double secondNumber = 2000000000;
        double resultOne = firstNumber + secondNumber;

        //This is the second part
        long firstLong = 10000;
        long secondLong = 10000;
        int result = (int)secondLong * (int)firstLong;

        //This is the third part
        int first = 8;
        int second = 5;
        int fSum = first + second;
        int fDiff = first - second;
        int fProd = first * second;
        int fQuot = first / second;
        int fQuottwo = fQuot / second;

        //this is the fourth part
        int resultPArtThreeOne = ++first;
        int resultPartThreeTwo = first++;
        int resultPartThreeThree = --second;
        int resultPartThreeFour = second--;

        System.out.println("The sum of firstNumber and secondNumber is = " + resultOne);
        System.out.println(" ");
        System.out.println("The multiplication of firstLong and secondLong is = " + result);
        System.out.println(" ");
        System.out.println("This is the sum of the two intergrals = " + fSum);
        System.out.println("This is the difference of the two intergrals = " + fDiff);
        System.out.println("This is the product of the two intergrals = " + fProd);
        System.out.println("This is the first quotient  of the two intergrals = " + fQuot);
        System.out.println("This is the second quotient of the two intergrals = " + fQuottwo);
        System.out.println(" ");
        System.out.println("With the ++first, you will increment 8 and then use it which results in " + resultPArtThreeOne);
        System.out.println("With the first++ you will use and then increment 8 which results in " + resultPartThreeTwo);
        System.out.println("With --second you will first decrement and then use it which results in " + resultPartThreeThree);
        System.out.println("with second-- you will fist use and then decrement 8, which results in " + resultPartThreeFour);

        return;
    }
}
