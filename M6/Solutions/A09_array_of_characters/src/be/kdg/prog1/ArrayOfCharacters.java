package be.kdg.prog1;

public class ArrayOfCharacters {
    public static void main(String[] args) {
        String word = "JavaScript";

        char[] letters = word.toCharArray();

        for (char letter : letters) {
            System.out.print(letter + " ");
        }
    }
}
