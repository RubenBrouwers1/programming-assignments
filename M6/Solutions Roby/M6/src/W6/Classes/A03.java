package W6.Classes;

public class A03 {
    public static void main(String[] args) {
        int[] lotteryNumbers = {3, 6, 17, 31, 32, 43};

        //print each number in a for each loop
        for(int number: lotteryNumbers){
            System.out.print(number + " ");
        }

        //edit second entry in array (java arrays are zero based --> lotteryNumbers[0] = the first entry)
        lotteryNumbers[1] = 13;
        System.out.println();
        //print edited array in a for each loop
        for(int number: lotteryNumbers){
            System.out.print(number + " ");
        }
    }
}
