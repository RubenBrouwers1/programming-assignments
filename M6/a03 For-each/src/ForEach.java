public class ForEach {
    public static void main(String[] args) {
        int[] lotteryNumbers = {3, 6, 17, 31, 32, 43};

        //print each number in array
        for (int lotterNumber : lotteryNumbers) {
            System.out.print(lotterNumber + " ");
        }

        lotteryNumbers[1] = 13;
        System.out.println();
        //print each number in array
        for (int lotterNumber : lotteryNumbers) {
            System.out.print(lotterNumber + " ");
        }
    }
}
