package be.kdg.prog1.m1.A11_Sequence;

import java.util.Scanner;

public class A11_Sequence {
    public static void main(String[] args) {

        int amount, startingValue, increment;
        Scanner scanner = new Scanner(System.in);

        System.out.print("How many numbers do you want to print? ");
        amount = scanner.nextInt();

        System.out.print("What is the starting value? ");
        startingValue = scanner.nextInt();

        System.out.print("What is the increment? ");
        increment = scanner.nextInt();

        System.out.println(" ");

        for (int i = 0; i < amount; i++) {
            System.out.println(startingValue);
            startingValue += increment;
        }
    }
}