package be.kdg.prog1.a13Between;

import java.util.InputMismatchException;
import java.util.Scanner;

public class Between {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        int max = 100;
        boolean correctNumb = false;
        int firstNum = 0, secondNum = 0, thirdNum = 0, middleNum = 1;

        System.out.print("Enter the first number (0..100): ");
        do {
            firstNum = sc.nextInt();
        } while (!correctNumb(firstNum));

        System.out.print("Enter the second number (0..100): ");
        do {
            secondNum = sc.nextInt();
        } while (!correctNumb(secondNum));

        System.out.print("Enter the third number (0..100): ");
        do {
            thirdNum = sc.nextInt();
        } while (!correctNumb(thirdNum));

        /*
        if (firstNum > secondNum && firstNum < thirdNum) {
            middleNum = firstNum;
        } else
        if (secondNum > firstNum && secondNum < thirdNum){
            middleNum = secondNum;
        } else
        if (thirdNum > firstNum && thirdNum < secondNum){
            middleNum = thirdNum;
        }
        System.out.printf("The middle number is %d", middleNum);
         */

        /*
        Roby

        if ((a - b) * (c - a) >= 0) {
            System.out.println(a + " is the middle number.");
        } //if a > b and c > a then it returns a number greater than 0 (ex. 10 - 15) * (20-10) = -50 --> not middle; (ex. 30 - 10) * (40 - 30) = 200 --> middle
        else if ((b - a) * (c - b) >= 0) {
            System.out.println(b + " is the middle number.");
        } //same case as above if --> b = middle
        else {
            //if both numbers return a negative value then c is the middle number
            System.out.println(c + " is the middle number.");
        }
         */

        int median = Math.max(Math.min(firstNum,secondNum), Math.min(Math.max(firstNum,secondNum),thirdNum));
        System.out.println("The middle number is: " + median);

    }

    static boolean correctNumb(int num) {
        int max = 100;
        int min = 1;
        boolean correctNumb = false;

        if (num > max || num < min) {
            System.out.print("Your number is out of range, please try again: ");
            correctNumb = false;
        } else {
            correctNumb = true;
        }
        return correctNumb;
    }
}
