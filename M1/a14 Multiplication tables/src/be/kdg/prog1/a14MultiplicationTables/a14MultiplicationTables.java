package be.kdg.prog1.a14MultiplicationTables;

import java.util.Scanner;

public class a14MultiplicationTables {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        int num, practice;
        boolean ansCorrect = true;

        System.out.print("Which mutliplication table would you want to see? ");
        num = sc.nextInt();

        for (int i = 1; i <= 10; i++) {
            System.out.println( i + " * " + num + " = " + i*num);
        }

        System.out.println();

        System.out.print("Which mutliplication table would you want to practice? ");
        practice = sc.nextInt();

        for (int i = 1; i <= 10; i++) {
            do {
                System.out.print( i + " * " + num + " = ");
                if (sc.nextInt() == (i * num)) {
                    System.out.println("Correct!");
                    ansCorrect = true;
                } else {
                    System.out.println("False!");
                    ansCorrect = false;
                }
            } while (!ansCorrect);
        }
    }
}
