package be.kdg.prog1.m8.MorningClass;

public class Helicopter extends Vehicle {
    private int verticalSpeed;

    public Helicopter(int verticalSpeed, int topSpeed) {
        super(topSpeed);
        this.verticalSpeed = verticalSpeed;
    }

    @Override
    public void print() {
            System.out.println("TOPSPEED: " +getTopSpeed());
        }
    }

