package be.kdg.prog1.m8.MorningClass;

public class Main {
    public static void main(String[] args) {
        Car car1 = new Car(200, false);
        Car car2 = new Car(220, true);
        Car car3 = new Car(230, false);

        System.out.println(car3.isElectric);
        System.out.println(Car.WHEELS);
        System.out.println(Car.hasEngine());

        //In memory we will have 3 ints, and 3 booleans.
        //Wheels is only in memory ones.
    }
}
