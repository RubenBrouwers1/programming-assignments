package be.kdg.prog1.m8.MorningClass;

public class Plane extends Vehicle implements Wheeled{
    public Plane(int topSpeed) {
        super(topSpeed);
    }

    @Override
    public void print() {
        System.out.println("TOPSPEED: " +getTopSpeed());
    }

    @Override
    public int getNumberOfWheels() {
        return 3;
    }
}
