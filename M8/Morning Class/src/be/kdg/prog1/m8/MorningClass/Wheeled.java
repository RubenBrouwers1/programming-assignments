package be.kdg.prog1.m8.MorningClass;

public interface Wheeled {
    /*public abstract*/ int getNumberOfWheels();
    void print();
    //Is a listing of multiple abstract methods, that should be implemented in different places
}
