package be.kdg.prog1.a06Input;
import java.util.InputMismatchException;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        boolean inputOk = false;
        boolean inputOk2 = false;

        //Change the int --> double

        do {
            try {
                do {
                    System.out.print("Please enter a decimal number between 0 and 10: ");
                    double number = sc.nextDouble();
                    if (number > 0 && number < 10) {
                        System.out.printf("Thanks, %.1f is a valid number!", number);
                        //System.out.print("Thanks, " + number + " is a valid number!");
                        inputOk2 = true;
                    } else {
                        System.out.println("That is not a number between 0 and 10!");
                        inputOk2 = false;
                    }
                }while (!inputOk2);
                inputOk = true;
            } catch (InputMismatchException e) {
                System.out.println("That is not a decimal number!");
                sc.nextLine(); //clear buffer?
                inputOk = false;
            }
        } while (!inputOk);
    }
}


        /*
        try {
            System.out.print("Please give me a floating point number between 0 and 1O: ");
            float number = sc.nextFloat();
            if (number < 0 || number < 10) {
                System.out.print("The number is out of range");
                return;
            }

        } catch (InputMismatchException e){
            System.out.println("That is not a valid input");
        }
         */


