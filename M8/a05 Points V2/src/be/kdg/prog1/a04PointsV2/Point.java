package be.kdg.prog1.a04PointsV2;

public class Point {
    private int x;
    private int y;

    private final static String color = "red";

    private static int count;
    {
        count++;
    }
    //Static integer values are always automatically initialize to zero

    public Point(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public static String getColor() {
        return color;
    }

    public static int getCount() {
        return count;
    }
}


