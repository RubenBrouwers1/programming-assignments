package be.kdg.prog1.a04PointsV2;

public class PointDemo {
    public static void main(String[] args) {
        System.out.println("--");

        Point p1 = new Point(1, 5);
        System.out.println("I have created " + Point.getCount()  + " objects.");
        System.out.println("The color is " + Point.getColor());
        System.out.println();

        Point p2 = new Point(2, 6);
        System.out.println("I have created " + Point.getCount() + " objects.");
        System.out.println("The color is " + Point.getColor());
        System.out.println();

        Point p3 = new Point(3, 7);
        System.out.println("I have created " + Point.getCount() + " objects.");
        System.out.println("The color is " + Point.getColor());
        System.out.println();

        Point p4 = new Point(4, 8);
        System.out.println("I have created " + Point.getCount() + " objects.");
        System.out.println("The color is " + Point.getColor());
        System.out.println();

        Point p5 = new Point(9, 9);
        System.out.println("I have created " + Point.getCount() + " objects.");
        System.out.println("The color is " + Point.getColor());
        System.out.println();

    }
}
